### Installation

```sudo apt-get install rxvt-unicode xsel```

then `git clone https://github.com/muennich/urxvt-perls`. Enter the urxvt-perls folder, then copy *keyboard-select, clipboard*, and *url-select* to *~/.urxvt/ext*
